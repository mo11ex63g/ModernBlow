#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ModernBlowGameModeBase.generated.h"

UCLASS(minimalapi)
class AModernBlowGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AModernBlowGameModeBase();
};
