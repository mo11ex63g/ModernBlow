#include "MBBaseCharacterAnimInstance.h"
#include "CharacterEquipComponent.h"
#include "KismetAnimationLibrary.h"
#include "MBBaseCharacter.h"
#include "MBPLayerCharacter.h"
#include "RangeWeaponItem.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PawnMovementComponent.h"

void UMBBaseCharacterAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();
	checkf(TryGetPawnOwner()->IsA<AMBPlayerCharacter>(), TEXT("TAtatatat"));
	CachedBaseCharacter = StaticCast<AMBPlayerCharacter*>(TryGetPawnOwner());
}

void UMBBaseCharacterAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	if(!CachedBaseCharacter.IsValid()) { return; }

	bIsAiming = CachedBaseCharacter->IsAiming();
	
	UCharacterMovementComponent* CharacterMovement = CachedBaseCharacter->GetCharacterMovement();
	Speed = CharacterMovement->Velocity.Size();
	bIsFalling = CharacterMovement->IsFalling();
	bIsCrouching = CharacterMovement->IsCrouching();
	bIsSprinting = false;
	bIsOutOfStamina = false;
	bIsStrafing = !CharacterMovement->bOrientRotationToMovement;
	Direction = UKismetAnimationLibrary::CalculateDirection(CharacterMovement->Velocity, CachedBaseCharacter->GetActorRotation());

	const UCharacterEquipComponent* CharacterEquipment = CachedBaseCharacter->GetCharacterEquipmentComponent();
	CurrentEquippedItem = CharacterEquipment->GetCurrentEquippedItemType();

	AimRotation = CachedBaseCharacter->GetBaseAimRotation();

	ARangeWeaponItem* CurrentRangeWeapon = CharacterEquipment->GetCurrentRangeWeapon();
	if(IsValid(CurrentRangeWeapon))
	{
		GripSocketTransform = CurrentRangeWeapon->GetForGripTransform();
	}
}
