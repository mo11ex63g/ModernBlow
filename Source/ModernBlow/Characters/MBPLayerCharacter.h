#pragma once

#include "CoreMinimal.h"
#include "EnhancedInputComponent.h"
#include "MBBaseCharacter.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"
#include "GASSystem/Character/MBPlayerState.h"
#include "GASSystem/Data/MBInputConfig.h"
#include "MBPLayerCharacter.generated.h"

struct FEnhancedInputActionEventBinding;
class UCharacterInventoryComponent;
class UInventoryItem;
class IInteractable;
class UTextRenderComponent;
class UInputAction;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnAimingStateChaged, bool)
DECLARE_DELEGATE_OneParam(FOnInteractableObjectFound, FName);

UCLASS(config=Game)
class AMBPlayerCharacter : public AMBBaseCharacter
{
	GENERATED_BODY()

public:
	AMBPlayerCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void Tick(float DeltaSeconds) override;

	void Move(const FInputActionValue& Value);
	void Look(const FInputActionValue& Value);

	void StartCrouch();
	void StopCrouch();

	void StartSprint();
	void StopSprint();
	
	void StartFire();
	void StopFire();

	bool bIsView = false;
	
	//float GetAimingMovementSpeed() const;
	
	void StartAim();
	void StopAim();

	bool IsAiming() const { return bIsAiming; }

	FOnAimingStateChaged OnAimingStateChaged;
	FOnInteractableObjectFound OnInteractableObjectFound;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Character")
	void OnStartAiming();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Character")
	void OnStopAiming();

	void Interact();

	bool PickupItem(TWeakObjectPtr<UInventoryItem> ItemToPickup);

	void UseInventory();

	void NextItem();
	void PreviousItem();

	void AbilityInputTagPressed(FGameplayTag InputTag);
	void AbilityInputTagReleased(FGameplayTag InputTag);
	void AbilityInputTagHold(FGameplayTag InputTag);
	
	UMBHealthComponent* GetHealthComponent() const;

	virtual void PossessedBy(AController* NewController) override;
	
	bool ASCInputBound = false;

	FGameplayTag DeadTag;

	void SendLocalInputToASC(bool bIsPressed, const EMBAbilityID AbilityID);
	
protected:
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void OnDeath() override;
	virtual void OnHealthChanged(float Health) override;
	virtual void OnRep_PlayerState() override;
	void InitializeStartingValues(AMBPlayerState* PS);
	
	virtual void OnStartAimingInternal();
	virtual void OnStopAimingInternal();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Components")
	UTextRenderComponent* TextRenderComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character | Team")
	float LineOfSightDistance = 1000.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character | Team")
	float CheckSphereRadius = 10.0f;
	
	void TraceLineOfSight();

	UPROPERTY()
	TScriptInterface<IInteractable> LineOfSightObject;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | Components")
	UCharacterInventoryComponent* CharacterInventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | Components")
	UMBHealthComponent* CharacterHealthComponent;
	
private:
	
	bool bIsAiming;
	float CurrentAimingMovementSpeed; 
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputMappingContext* DefaultMappingContext;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* JumpAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* LookAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* FireAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* CrouchAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* AimAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* SprintAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* InteractAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* InventoryAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* NextItemAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* PreviousItemAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* AbilityAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UMBInputConfig> InputConfig;
};


