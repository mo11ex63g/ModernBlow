#include "MBPLayerCharacter.h"
#include "CharacterEquipComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "RangeWeaponItem.h"
#include "Components/MBHealthComponent.h"
#include "Components/TextRenderComponent.h"
#include "Engine/DamageEvents.h"
#include "ModernBlow/Interactive.h"
#include "CharacterInventoryComponent.h"
#include "Chaos/Deformable/ChaosDeformableCollisionsProxy.h"
#include "Components/SphereComponent.h"
#include "GASSystem/Character/MBPlayerState.h"
#include "GASSystem/Character/Abilities/CharacterAbilitySystemComponent.h"
#include "Input/MBInputComponent.h"

AMBPlayerCharacter::AMBPlayerCharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f);
	
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;
	
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f; 	
	CameraBoom->bUsePawnControlRotation = true;
	
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;
	
	TextRenderComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextRenderComponent"));
	TextRenderComponent->SetupAttachment(GetRootComponent());
	
	CharacterInventoryComponent = CreateDefaultSubobject<UCharacterInventoryComponent>(TEXT("InventoryComponent"));

	DeadTag = FGameplayTag::RequestGameplayTag(FName("State.Dead"));
}

void AMBPlayerCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	TraceLineOfSight();
}

void AMBPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

void AMBPlayerCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if(OnInteractableObjectFound.IsBound())
	{
		OnInteractableObjectFound.Unbind();
	}
	Super::EndPlay(EndPlayReason);
}

void AMBPlayerCharacter::OnDeath()
{
	Super::OnDeath();
}

void AMBPlayerCharacter::OnHealthChanged(float Health)
{
	Super::OnHealthChanged(Health);
	TextRenderComponent->SetText(FText::FromString(FString::Printf(TEXT("%0.f"), Health)));
}

void AMBPlayerCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	AMBPlayerState* PS = GetPlayerState<AMBPlayerState>();
	if(PS)
	{
		InitializeStartingValues(PS);
	}
}

void AMBPlayerCharacter::InitializeStartingValues(AMBPlayerState* PS)
{
	AbilitySystemComponent = Cast<UCharacterAbilitySystemComponent>(PS->GetAbilitySystemComponent());
	PS->GetAbilitySystemComponent()->InitAbilityActorInfo(PS, this);
	AttributeSetBase = PS->GetAttributeSetBase();
	AbilitySystemComponent->SetTagMapCount(DeadTag, 0);
	InitializeAttributes();
	SetHealth(GetMaxHealth());
	SetMana(GetMaxMana());
}

void AMBPlayerCharacter::OnStartAimingInternal()
{
	if(OnAimingStateChaged.IsBound())
	{
		OnAimingStateChaged.Broadcast(true);
	}
	
	APlayerController* PlayerController = GetController<APlayerController>();
	if(!IsValid(PlayerController))
	{
		return;
	}

	APlayerCameraManager* Camera = PlayerController->PlayerCameraManager;
	if(IsValid(Camera))
	{
		ARangeWeaponItem* CurrentRangeWeapon = CharacterEquipComponent->GetCurrentRangeWeapon();
		Camera->SetFOV(CurrentRangeWeapon->GetAimFOV());
	}
}

void AMBPlayerCharacter::OnStopAimingInternal()
{
	if(OnAimingStateChaged.IsBound())
	{
		OnAimingStateChaged.Broadcast(false);
	}
	
	APlayerController* PlayerController = GetController<APlayerController>();
	if(!IsValid(PlayerController))
	{
		return;
	}

	APlayerCameraManager* Camera = PlayerController->PlayerCameraManager;
	if(IsValid(Camera))
	{
		ARangeWeaponItem* CurrentRangeWeapon = CharacterEquipComponent->GetCurrentRangeWeapon();
		Camera->UnlockFOV();
	}
}

void AMBPlayerCharacter::TraceLineOfSight()
{
	bIsView = false;
	
	FVector ViewLocation;
	FRotator ViewRotation;

	AController* PlayerController = GetController();
	PlayerController->GetPlayerViewPoint(ViewLocation, ViewRotation);

	FVector ViewDirection = ViewRotation.Vector();
	FVector TraceEnd = ViewLocation + ViewDirection * LineOfSightDistance;

	FCollisionQueryParams QueryParams;
	QueryParams.bTraceComplex = true;
	QueryParams.AddIgnoredActor(GetOwner());
	
	FCollisionShape CheckSphere = FCollisionShape::MakeSphere(CheckSphereRadius);
	
	FHitResult HitResult;
	//GetWorld()->LineTraceSingleByChannel(HitResult, ViewLocation, TraceEnd, ECC_Visibility);
	GetWorld()->SweepSingleByChannel(HitResult, ViewLocation, TraceEnd, FQuat::Identity, ECC_Visibility, CheckSphere, QueryParams);
	if (LineOfSightObject.GetObject() != HitResult.GetActor())
	{
		LineOfSightObject = HitResult.GetActor();
		
		FName ActionName;
		if (LineOfSightObject.GetInterface())
		{
			ActionName = LineOfSightObject->GetActionEventName();
			bIsView = true;
		}
		else
		{
			ActionName = NAME_None;
		}
		OnInteractableObjectFound.ExecuteIfBound(ActionName);
	}
}

void AMBPlayerCharacter::StartFire()
{
	ARangeWeaponItem* CurrentRangeWeapon = CharacterEquipComponent->GetCurrentRangeWeapon();
	if(IsValid(CurrentRangeWeapon))
	{
		CurrentRangeWeapon->StartFire();
	}
}

void AMBPlayerCharacter::StopFire()
{
	ARangeWeaponItem* CurrentRangeWeapon = CharacterEquipComponent->GetCurrentRangeWeapon();
	if(IsValid(CurrentRangeWeapon))
	{
		CurrentRangeWeapon->StopFire();
	}
}

/*float AMBPlayerCharacter::GetAimingMovementSpeed() const
{
	float CurrentAimingMovementSpeed = 0.0f;
	
	return CurrentAimingMovementSpeed;
}*/

void AMBPlayerCharacter::StartAim()
{
	ARangeWeaponItem* CurrentRangeWeapon = GetCharacterEquipmentComponent()->GetCurrentRangeWeapon();
	if(!IsValid(CurrentRangeWeapon))
	{
		return;
	}

	GetCharacterMovement()->bOrientRotationToMovement = false;
	bUseControllerRotationYaw = true;	
	
	bIsAiming = true;
	CurrentAimingMovementSpeed = CurrentRangeWeapon->GetAimMovementMaxSpeed();
	GetCharacterMovement()->MaxWalkSpeed = CurrentAimingMovementSpeed;
	CurrentRangeWeapon->StartAim();
	OnStartAiming();
}

void AMBPlayerCharacter::StopAim()
{
	if(!bIsAiming)
	{
		return;
	}

	ARangeWeaponItem* CurrentRangeWeapon = GetCharacterEquipmentComponent()->GetCurrentRangeWeapon();
	if(IsValid(CurrentRangeWeapon))
	{
		CurrentRangeWeapon->StopAim();
	}

	GetCharacterMovement()->bOrientRotationToMovement = true;
	bUseControllerRotationYaw = false;
	
	bIsAiming = false;
	CurrentAimingMovementSpeed = 0.0f;
	GetCharacterMovement()->MaxWalkSpeed = 450.0f;
	OnStopAiming();
}

void AMBPlayerCharacter::OnStartAiming_Implementation()
{
	OnStartAimingInternal();
}

void AMBPlayerCharacter::OnStopAiming_Implementation()
{
	OnStopAimingInternal();
}

void AMBPlayerCharacter::Interact()
{
	if(LineOfSightObject.GetInterface())
	{
		LineOfSightObject->Interact(this);
		UE_LOG(LogClass, Warning, TEXT("Yes"));
	}
	else
	{
		UE_LOG(LogClass, Warning, TEXT("No"));
	}
}

bool AMBPlayerCharacter::PickupItem(TWeakObjectPtr<UInventoryItem> ItemToPickup)
{
	bool Result = false;
	if(CharacterInventoryComponent->HasFreeSlot())
	{
		CharacterInventoryComponent->AddItem(ItemToPickup, 1);
		Result = true;
	}
	return Result;
}

void AMBPlayerCharacter::UseInventory()
{
	if(!IsPlayerControlled())
	{
		return;
	}

	APlayerController* PlayerController = GetController<APlayerController>();

	if(!CharacterInventoryComponent->IsViewVisible())
	{
		CharacterInventoryComponent->OpenViewInventory(PlayerController);
		CharacterEquipComponent->OpenViewEquipment(PlayerController);
		PlayerController->SetInputMode(FInputModeGameAndUI {});
		PlayerController->bShowMouseCursor = true;
	}
	else
	{
		CharacterInventoryComponent->CloseViewInventory();
		CharacterEquipComponent->CloseViewEquipment();
		PlayerController->SetInputMode(FInputModeGameOnly {});
		PlayerController->bShowMouseCursor = false;
	}
}

void AMBPlayerCharacter::NextItem()
{
	CharacterEquipComponent->EquipNextItem();
}

void AMBPlayerCharacter::PreviousItem()
{
	CharacterEquipComponent->EquipPreviousItem();
}

void AMBPlayerCharacter::AbilityInputTagPressed(FGameplayTag InputTag)
{
	//GEngine->AddOnScreenDebugMessage(1, 3.f, FColor::Red, *InputTag.ToString());
}

void AMBPlayerCharacter::AbilityInputTagReleased(FGameplayTag InputTag)
{
	if(GetAbilitySystemComponent() == nullptr) return;
	GetAbilitySystemComponent()->AbilityInputTagReleased(InputTag);
}

void AMBPlayerCharacter::AbilityInputTagHold(FGameplayTag InputTag)
{
	if(GetAbilitySystemComponent() == nullptr) return;
	GetAbilitySystemComponent()->AbilityInputTagHold(InputTag);
}

UMBHealthComponent* AMBPlayerCharacter::GetHealthComponent() const
{
	return HealthComponent;
}

void AMBPlayerCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	AMBPlayerState* PS = GetPlayerState<AMBPlayerState>();
	if(PS)
	{
		InitializeStartingValues(PS);
		AddStartupEffects();
		AddCharacterAbilities();
	}
}


void AMBPlayerCharacter::SendLocalInputToASC(bool bIsPressed, const EMBAbilityID AbilityID)
{
	if(!AbilitySystemComponent.IsValid())
	{
		return;
	}
	
	if(bIsPressed)
	{
		AbilitySystemComponent->AbilityLocalInputPressed(static_cast<int32>(AbilityID));
		UE_LOG(LogTemp, Warning, TEXT("SendLocalInputToASC pressed"));
	}
	else
	{
		AbilitySystemComponent->AbilityLocalInputReleased(static_cast<int32>(AbilityID));
		UE_LOG(LogTemp, Warning, TEXT("SendLocalInputToASC released"));
	}
}

void AMBPlayerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	if (UMBInputComponent* MBInputComponent = CastChecked<UMBInputComponent>(PlayerInputComponent))
	{			
		MBInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ACharacter::Jump);
		MBInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);
		
		MBInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AMBPlayerCharacter::Move);
		
		MBInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &AMBPlayerCharacter::Look);

		MBInputComponent->BindAction(FireAction, ETriggerEvent::Started, this, &AMBPlayerCharacter::StartFire);
		MBInputComponent->BindAction(FireAction, ETriggerEvent::Completed, this, &AMBPlayerCharacter::StopFire);
		
		MBInputComponent->BindAction(AimAction, ETriggerEvent::Started, this, &AMBPlayerCharacter::StartAim);
		MBInputComponent->BindAction(AimAction, ETriggerEvent::Completed, this, &AMBPlayerCharacter::StopAim);

		MBInputComponent->BindAction(CrouchAction, ETriggerEvent::Started, this, &AMBPlayerCharacter::StartCrouch);
		MBInputComponent->BindAction(CrouchAction, ETriggerEvent::Completed, this, &AMBPlayerCharacter::StopCrouch);

		MBInputComponent->BindAction(SprintAction, ETriggerEvent::Started, this, &AMBPlayerCharacter::StartSprint);
		MBInputComponent->BindAction(SprintAction, ETriggerEvent::Completed, this, &AMBPlayerCharacter::StopSprint);

		MBInputComponent->BindAction(NextItemAction, ETriggerEvent::Triggered, this, &AMBPlayerCharacter::NextItem);
		MBInputComponent->BindAction(PreviousItemAction, ETriggerEvent::Triggered, this, &AMBPlayerCharacter::PreviousItem);
		
		MBInputComponent->BindAction(InteractAction, ETriggerEvent::Triggered, this, &AMBPlayerCharacter::Interact);
		
		MBInputComponent->BindAction(InventoryAction, ETriggerEvent::Triggered, this, &AMBPlayerCharacter::UseInventory);
		
		MBInputComponent->BindAbilityActions(InputConfig, this, &ThisClass::AbilityInputTagPressed, &ThisClass::AbilityInputTagReleased, &ThisClass::AbilityInputTagHold);
	}
}

void AMBPlayerCharacter::Move(const FInputActionValue& Value)
{
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		
		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
	}
}

void AMBPlayerCharacter::Look(const FInputActionValue& Value)
{
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void AMBPlayerCharacter::StartCrouch()
{
	Crouch();
	//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Cyan, TEXT("Is Crouching!"));
}

void AMBPlayerCharacter::StopCrouch()
{
	UnCrouch();
	//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Cyan, TEXT("Is UnCrouching!"));	
}

void AMBPlayerCharacter::StartSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = 900.0f;
}

void AMBPlayerCharacter::StopSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = 450.0f;
}


