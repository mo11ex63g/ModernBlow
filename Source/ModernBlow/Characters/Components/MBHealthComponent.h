#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MBHealthComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnDeath);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnHealthChanged, float);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MODERNBLOW_API UMBHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UMBHealthComponent();
	
	FORCEINLINE float GetHealth() const { return Health; }

	UFUNCTION(BlueprintCallable, Category = "Helath")
	float GetHealthPercent() { return Health / MaxHealth; }

	UFUNCTION(BlueprintCallable)
	bool IsDead() const { return FMath::IsNearlyZero(Health); }

	void AddHealth(float HealthToAdd);

	void SetHealth(float NewHealth);
	
	FOnDeath OnDeath;
	FOnHealthChanged OnHealthChanged;
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (ClampMin = 0.0f, UIMin = 0.0f))
	float MaxHealth = 100.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta=(EditCondition="AutoHeal"))
	bool AutoHeal = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta=(EditCondition="AutoHeal"))
	float HealUpdateTime = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta=(EditCondition="AutoHeal"))
	float HealDelay = 10.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta=(EditCondition="AutoHeal"))
	float HealModifier = 1.0f;
	
	virtual void BeginPlay() override;
	
private:
	float Health = 0.0f;

	FTimerHandle HealTimerHandle;
	
	UFUNCTION()
	void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

	void HealUpdate();

	TWeakObjectPtr<class UCharacterAttributeSetBase> AttributeSetBase;
};
