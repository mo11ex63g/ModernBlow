#include "MBHealthComponent.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "VectorUtil.h"
#include "GASSystem/Character/Abilities/AttributeSets/CharacterAttributeSetBase.h"

UMBHealthComponent::UMBHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UMBHealthComponent::AddHealth(float HealthToAdd)
{
	SetHealth(FMath::Clamp(Health + HealthToAdd, 0, MaxHealth));
	if(Health <= 0.0f)
	{
		if(OnDeath.IsBound())
		{
			OnDeath.Broadcast();
		}
	}
}

void UMBHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	SetHealth(MaxHealth);

	AActor* ComponentOwner = GetOwner();
	if(ComponentOwner)
	{
		ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &UMBHealthComponent::OnTakeAnyDamage);
	}
}

void UMBHealthComponent::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	if(Damage <= 0.0f || IsDead() || !GetWorld())	return;
	
	SetHealth(Health - Damage);
	//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, FString::Printf(TEXT("- %2.f hp damage"), Damage));

	GetWorld()->GetTimerManager().ClearTimer(HealTimerHandle);
	
	if(IsDead())
	{
		OnDeath.Broadcast();
	}

	else if (AutoHeal && GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(HealTimerHandle, this, &UMBHealthComponent::HealUpdate, HealUpdateTime, true, HealDelay);
	}
}

void UMBHealthComponent::HealUpdate()
{
	SetHealth(Health + HealModifier);

	if(FMath::IsNearlyEqual(Health,MaxHealth) && GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(HealTimerHandle);
	}
}

void UMBHealthComponent::SetHealth(float NewHealth)
{
	Health = FMath::Clamp(NewHealth, 0.0f, MaxHealth);
	OnHealthChanged.Broadcast(Health);
}

