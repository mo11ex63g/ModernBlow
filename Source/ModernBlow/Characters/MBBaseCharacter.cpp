#include "MBBaseCharacter.h"
#include "Components/MBHealthComponent.h"
//#include "ActionComponent.h"
#include "CharacterEquipComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Controller.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/DamageEvents.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GASSystem/Character/Abilities/CharacterAbilitySystemComponent.h"
#include "GASSystem/Character/Abilities/AttributeSets/CharacterAttributeSetBase.h"
#include "GASSystem/Character/Abilities/CharacterGameplayAbility.h"

DEFINE_LOG_CATEGORY_STATIC(LogBaseCharacter, All, All);

AMBBaseCharacter::AMBBaseCharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer.SetDefaultSubobjectClass<UCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	PrimaryActorTick.bCanEverTick = true;
	HealthComponent = CreateDefaultSubobject<UMBHealthComponent>("HealthComponent");
	CharacterEquipComponent = CreateDefaultSubobject<UCharacterEquipComponent>(TEXT("CharacterEquipment"));

	DeadTag = FGameplayTag::RequestGameplayTag(FName("State.Dead"));
	EffectRemoveOnDeathTag = FGameplayTag::RequestGameplayTag(FName("State.RemoveOnDeath"));

	bAlwaysRelevant = true;

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Overlap);

	AbilitySystemComponent = CreateDefaultSubobject<UCharacterAbilitySystemComponent>("AbilitySystemComponent");
	AbilitySystemComponent->SetIsReplicated(true);
	
	//ActionComponent = CreateDefaultSubobject<UActionComponent>("ActionComponent");
}

void AMBBaseCharacter::AddCharacterAbilities()
{
	if(!HasAuthority()) return;

	GetAbilitySystemComponent()->AddCharacterAbilities(CharacterAbilities);
	AbilitySystemComponent->CharacterAbilitiesGiven = true;
}

void AMBBaseCharacter::InitializeAttributes()
{
	if(!AbilitySystemComponent.IsValid())
	{
		return;
	}

	if(!DefaultAttributes)
	{
		UE_LOG(LogTemp, Error, TEXT("%s() Missing DefaultAttributes for %s. Please fill in the Character`s blueprint."), *FString(__FUNCTION__), *GetName());
		return;
	}

	FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
	EffectContext.AddSourceObject(this);

	FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(DefaultAttributes, GetCharacterLevel(), EffectContext);
	if(NewHandle.IsValid())
	{
		FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), AbilitySystemComponent.Get());
	}
}

void AMBBaseCharacter::AddStartupEffects()
{
	if(GetLocalRole() != ROLE_Authority || !AbilitySystemComponent.IsValid() || AbilitySystemComponent->StartupsEffectsApplied)
	{
		if(GetLocalRole() != ROLE_Authority)
		{
			UE_LOG(LogTemp, Warning, TEXT("Not add startup effects: GetLocalRole() == ROLE_Authority!"));
			return;
		}
		if(!AbilitySystemComponent->StartupsEffectsApplied)
		{
			UE_LOG(LogTemp, Warning, TEXT("Not add startup effects: AbilitySystemComponent->StartupsEffectsApplied!"));
			return;
		}
		if(!AbilitySystemComponent.IsValid())
		{
			UE_LOG(LogTemp, Warning, TEXT("Not add startup effects: !AbilitySystemComponent.IsValid()!"));
			return;
		}
		return;
	}

	FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
	EffectContext.AddSourceObject(this);

	for(TSubclassOf<UGameplayEffect> GameplayEffect : StartupEffects)
	{
		FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(GameplayEffect, GetCharacterLevel(), EffectContext);
		if(NewHandle.IsValid())
		{
			FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), AbilitySystemComponent.Get());
		}	
	}

	AbilitySystemComponent->StartupsEffectsApplied = true;
	UE_LOG(LogTemp, Warning, TEXT("Add startup effects!"));
}

void AMBBaseCharacter::SetHealth(float Health)
{
	if(AttributeSetBase.IsValid())
	{
		AttributeSetBase->SetHealth(Health); 
	}
}

void AMBBaseCharacter::SetMana(float Mana)
{
	if(AttributeSetBase.IsValid())
	{
		AttributeSetBase->SetMana(Mana); 
	}
}

void AMBBaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	check(HealthComponent);
	//check(ActionComponent)
	check(GetCapsuleComponent());
	check(GetMesh());
	check(GetCharacterMovement());

	OnHealthChanged(HealthComponent->GetHealth());
	//OnCharacterDied.Broadcast(this);
	
	HealthComponent->OnDeath.AddUObject(this, &AMBBaseCharacter::OnDeath);
	HealthComponent->OnHealthChanged.AddUObject(this, &AMBBaseCharacter::OnHealthChanged);

	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetHealthAttribute()).AddUObject(this, &AMBBaseCharacter::OnHealthAttributeChanged);

	LandedDelegate.AddDynamic(this, &AMBBaseCharacter::OnGroundLanded);
}

void AMBBaseCharacter::OnDeath()
{
	RemoveCharacterAbilities();
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCharacterMovement()->GravityScale = 0;
	GetCharacterMovement()->DisableMovement();

	OnCharacterDied.Broadcast(this);

	if(AbilitySystemComponent.IsValid())
	{
		AbilitySystemComponent->CancelAbilities();
		FGameplayTagContainer EffectsTagsToRemove;
		EffectsTagsToRemove.AddTag(EffectRemoveOnDeathTag);
		int32 NumEffectsRemoved = AbilitySystemComponent->RemoveActiveEffectsWithTags(EffectsTagsToRemove);
		AbilitySystemComponent->AddLooseGameplayTag(DeadTag);
	}

	if(DeathAnimMontage)
	{
		PlayAnimMontage(DeathAnimMontage);
	}
	FinishDie();
}

void AMBBaseCharacter::FinishDie()
{
	SetLifeSpan(3.0f);
}

float AMBBaseCharacter::GetCharacterLevel() const
{
	if(AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetLevel();
	}

	return 0.0f;
}

float AMBBaseCharacter::GetHealth() const
{
	if(AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetHealth();
	}

	return 0.0f;
}

float AMBBaseCharacter::GetMana() const
{
	if(AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetMana();
	}

	return 0.0f;
}

float AMBBaseCharacter::GetMaxHealth() const
{
	if(AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetMaxHealth();
	}

	return 0.0f;
}

float AMBBaseCharacter::GetMaxMana() const
{
	if(AttributeSetBase.IsValid())
	{
		return AttributeSetBase->GetMaxMana();
	}

	return 0.0f;
}

void AMBBaseCharacter::OnHealthChanged(float Health)
{
}

void AMBBaseCharacter::OnGroundLanded(const FHitResult& Hit)
{
	const auto FallVelosityZ = -GetCharacterMovement()->Velocity.Z;

	if(FallVelosityZ < LandedDamageVelosity.X) return;

	const auto FinalDamage = FMath::GetMappedRangeValueClamped(LandedDamageVelosity, LandedDamage, FallVelosityZ);
	TakeDamage(FinalDamage, FDamageEvent {}, nullptr, nullptr);
}

void AMBBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

UCharacterEquipComponent* AMBBaseCharacter::GetCharacterEquipmentComponent() const
{
	return CharacterEquipComponent;
}

UCharacterAbilitySystemComponent* AMBBaseCharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent.Get();
}

bool AMBBaseCharacter::IsAlive() const
{
	return GetHealth() > 0.0f;
}

int32 AMBBaseCharacter::GetAbilityLevel(EMBAbilityID AbilityID) const
{
	return 1;
}

void AMBBaseCharacter::RemoveCharacterAbilities()
{
	if(GetLocalRole() != ROLE_Authority || !AbilitySystemComponent.IsValid() || !AbilitySystemComponent->CharacterAbilitiesGiven)
	{
		return;
	}

	TArray<FGameplayAbilitySpecHandle> AbilitiesToRemove;
	for(const FGameplayAbilitySpec& Spec : AbilitySystemComponent->GetActivatableAbilities())
	{
		if((Spec.SourceObject == this) && CharacterAbilities.Contains(Spec.Ability->GetClass()))
		{
			AbilitiesToRemove.Add(Spec.Handle);
		}
	}

	for(int32 i = 0; i < AbilitiesToRemove.Num(); i++)
	{
		AbilitySystemComponent->ClearAbility(AbilitiesToRemove[i]);
	}

	AbilitySystemComponent->CharacterAbilitiesGiven = false;
}

void AMBBaseCharacter::OnHealthAttributeChanged(const FOnAttributeChangeData& Data)
{
	HealthComponent->AddHealth(-(OldHealth - AttributeSetBase->GetHealth()));
	OldHealth = AttributeSetBase->GetHealth();
}


