#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameplayTagContainer.h"
#include "AbilitySystemInterface.h"
#include "GASSystem/Character/Abilities/CharacterAbilitySystemComponent.h"
#include "GASSystem/Character/Abilities/AttributeSets/CharacterAttributeSetBase.h"
#include "MBBaseCharacter.generated.h"

struct FGameplayTag;
enum class EMBAbilityID : uint8;
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCharacterDiedDelegate, AMBBaseCharacter*, Character);

class UCharacterEquipComponent;
class UMBHealthComponent;
//class UWeaponComponent;
//class UActionComponent;

//------------------------------------------------------------------------------------------------------------
UCLASS(config=Game)
class AMBBaseCharacter : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()
	
public:
	AMBBaseCharacter(const FObjectInitializer& ObjectInitializer);
	virtual void Tick(float DeltaTime) override;

	UCharacterEquipComponent* GetCharacterEquipmentComponent() const;

	virtual UCharacterAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UPROPERTY(BlueprintAssignable, Category = "Character")
	FCharacterDiedDelegate OnCharacterDied;

	UFUNCTION(BlueprintCallable, Category = "Character")
	virtual bool IsAlive() const;

	UFUNCTION(BlueprintCallable, Category = "Character")
	virtual int32 GetAbilityLevel(EMBAbilityID AbilityID) const;

	virtual void RemoveCharacterAbilities();

	void OnHealthAttributeChanged(const FOnAttributeChangeData& Data);
	
	virtual void OnDeath();

	UFUNCTION(BlueprintCallable, Category = "Character")
	virtual void FinishDie();

	UFUNCTION(BlueprintCallable, Category = "Character|Attributes")
	float GetCharacterLevel() const;
	
	UFUNCTION(BlueprintCallable, Category = "Character|Attributes")
	float GetHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Character|Attributes")
	float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Character|Attributes")
	float GetMana() const;

	UFUNCTION(BlueprintCallable, Category = "Character|Attributes")
	float GetMaxMana() const;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components", meta=(ClampMin = 0.0f, UIMin = 0.0f))
	UMBHealthComponent* HealthComponent;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Character")
	FText CharacterName;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Animation")
	UAnimMontage* DeathAnimMontage;

	UPROPERTY(EditDefaultsOnly, Category="Movement")
	FVector2D LandedDamageVelosity = FVector2D(900.0f, 1200.0f);

	UPROPERTY(EditDefaultsOnly, Category="Movement")
	FVector2D LandedDamage = FVector2D(10.0f, 100.0f);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCharacterEquipComponent* CharacterEquipComponent;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Character|Abilities")
	TArray<TSubclassOf<class UCharacterGameplayAbility>> CharacterAbilities;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Character|Abilities")
	TSubclassOf<class UGameplayEffect> DefaultAttributes;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Character|Abilities")
	TArray<TSubclassOf<class UGameplayEffect>> StartupEffects;

	virtual void AddCharacterAbilities();

	virtual void InitializeAttributes();

	virtual void AddStartupEffects();

	virtual void SetHealth(float Health);
	virtual void SetMana(float Mana);
	
	/*UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UWeaponComponent* WeaponComponent;*/

	/*UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UActionComponent* ActionComponent;*/
	
	virtual void BeginPlay() override;
	virtual void OnHealthChanged(float Health);

	UFUNCTION()
	void OnGroundLanded(const FHitResult& Hit);
	
	TWeakObjectPtr<class UCharacterAbilitySystemComponent> AbilitySystemComponent;
	
	TWeakObjectPtr<class UCharacterAttributeSetBase> AttributeSetBase;

	FGameplayTag DeadTag;
	FGameplayTag EffectRemoveOnDeathTag;

	float OldHealth = 100.0f;
};
