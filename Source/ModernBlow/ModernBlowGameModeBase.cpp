#include "ModernBlowGameModeBase.h"
#include "Characters/MBBaseCharacter.h"
#include "UI/GameHUD.h"
#include "UObject/ConstructorHelpers.h"

AModernBlowGameModeBase::AModernBlowGameModeBase()
{
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ModernBlow/Characters/BP_PlayerCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	else
	{
		DefaultPawnClass = AMBBaseCharacter::StaticClass();
	}
	HUDClass = AGameHUD::StaticClass();
}
