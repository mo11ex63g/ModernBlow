#include "GameHUD.h"
#include "AmmoWidget.h"
#include "CharacterEquipComponent.h"
#include "CrosshairWidget2.h"
#include "EnhancedInputSubsystemInterface.h"
#include "MBPLayerCharacter.h"
#include "Blueprint/UserWidget.h"
#include "GameFramework/PlayerInput.h"

void AGameHUD::DrawHUD()
{
	Super::DrawHUD();
}

void AGameHUD::BeginPlay()
{
	Super::BeginPlay();

	//const TSoftObjectPtr<AMBPlayerCharacter> CachedBaseCharacter;
	
	APlayerController* PlayerController = GetOwningPlayerController();
	if(PlayerController)
	{
		AMBPlayerCharacter* CachedBaseCharacter = Cast<AMBPlayerCharacter>(PlayerController->GetPawn());
		if(CachedBaseCharacter)
		{
			CreateAndInitializeWidgets();
			CachedBaseCharacter->OnInteractableObjectFound.BindUObject(this, &AGameHUD::OnInteractableObjectFound);
		}
		else
		{
			UE_LOG(LogAudio, Warning, TEXT("Character not valid"));
		}
	}
	else
	{
		UE_LOG(LogAudio, Warning, TEXT("Controller not valid"));
	}
}

void AGameHUD::OnInteractableObjectFound(FName ActionName)
{
	if (!IsValid(PlayerHUDWidget)) 
	{
		return;
	}

	APlayerController* PlayerController = GetOwningPlayerController();
	if(PlayerController)
	{
		AMBPlayerCharacter* CachedBaseCharacter = Cast<AMBPlayerCharacter>(PlayerController->GetPawn());
		PlayerHUDWidget->SetHightlightInteractableActionText(FName("E"));
		PlayerHUDWidget->SetHightlightInteractableVisability(CachedBaseCharacter->bIsView);
	}
	/*const UEnhancedPlayerInput* PlayerInput = NewObject<UEnhancedPlayerInput>();
	
	TArray<FEnhancedActionKeyMapping> ActionKeys = PlayerInput->GetEnhancedActionMappings();
	const bool HasAnyKeys = ActionKeys.Num() != 0;
	if (HasAnyKeys)
	{
		FName ActionKey = ActionKeys[0].Key.GetFName();
		PlayerHUDWidget->SetHightlightInteractableActionText(ActionKey);
	}*/
	
}

void AGameHUD::CreateAndInitializeWidgets()
{
	if(!IsValid(PlayerHUDWidget))
	{
		PlayerHUDWidget = CreateWidget<UPlyerWidget>(GetWorld(), PLayerWidgetClass);
		if(PlayerHUDWidget)
		{
			PlayerHUDWidget->AddToViewport();
		}
	}	
}
