#pragma once

#include "CoreMinimal.h"
#include "AmmoWidget.h"
#include "Blueprint/UserWidget.h"
#include "PlyerWidget.generated.h"

class UHigtlightInteractable;
class UCrosshairWidget2;

UCLASS()
class MODERNBLOW_API UPlyerWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	virtual void NativeConstruct() override;
	void SetHightlightInteractableVisability(bool bIsVisible);
	void SetHightlightInteractableActionText(FName KeyName);
	
protected:
	UFUNCTION(BlueprintCallable, Category = "UI")
	float GetHealthPercent() const;

	UPROPERTY(meta = (BindWidget))
	UHigtlightInteractable* InteractableKey;
};
