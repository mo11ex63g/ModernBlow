#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlyerWidget.h"
#include "CrosshairWidget2.generated.h"

UCLASS()
class MODERNBLOW_API UCrosshairWidget2 : public UPlyerWidget
{
	GENERATED_BODY()

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void OnAimingStateChanged(bool bIsAiming);
};
