#include "UI/HigtlightInteractable.h"

#include "Components/TextBlock.h"

void UHigtlightInteractable::SetActionText(FName KeyName)
{
	if(IsValid(ActionText))
	{
		ActionText->SetText(FText::FromName(KeyName));
	}
}
