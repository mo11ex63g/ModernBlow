#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HigtlightInteractable.generated.h"

class UTextBlock;

UCLASS()
class MODERNBLOW_API UHigtlightInteractable : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetActionText(FName KeyName);

protected:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* ActionText;
};
	