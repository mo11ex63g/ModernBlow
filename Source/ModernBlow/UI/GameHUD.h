#pragma once

#include "CoreMinimal.h"
#include "MBBaseCharacter.h"
#include "PlyerWidget.h"
#include "GameFramework/HUD.h"
#include "GameHUD.generated.h"

UCLASS()
class MODERNBLOW_API AGameHUD : public AHUD
{
	GENERATED_BODY()

public:
	virtual void DrawHUD() override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UPlyerWidget> PLayerWidgetClass;

	virtual void BeginPlay() override;
	
private:
	void OnInteractableObjectFound(FName ActionName);
	
	void CreateAndInitializeWidgets();
	
	UPlyerWidget* PlayerHUDWidget = nullptr;
};
