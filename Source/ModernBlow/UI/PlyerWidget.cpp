#include "PlyerWidget.h"
#include "CharacterEquipComponent.h"
#include "HigtlightInteractable.h"
#include "MBBaseCharacter.h"
#include "MBHealthComponent.h"
#include "Blueprint/WidgetTree.h"

void UPlyerWidget::NativeConstruct()
{
	Super::NativeConstruct();
	InteractableKey->SetVisibility(ESlateVisibility::Hidden);
}

void UPlyerWidget::SetHightlightInteractableVisability(bool bIsVisible)
{
	if(!IsValid(InteractableKey))
	{
		return;
	}
	if(bIsVisible)
	{
		InteractableKey->SetVisibility(ESlateVisibility::Visible);
	}
	else
	{
		InteractableKey->SetVisibility(ESlateVisibility::Hidden);
	}
}

void UPlyerWidget::SetHightlightInteractableActionText(FName KeyName)
{
	if(IsValid(InteractableKey))
	{
		InteractableKey->SetActionText(KeyName);
	}
}

float UPlyerWidget::GetHealthPercent() const
{
	const auto Player = GetOwningPlayerPawn();
	if(!Player)
	{
		return 0.0f;
	}

	const auto Component = Player->GetComponentByClass(UMBHealthComponent::StaticClass());
	const auto HealthComponent = Cast<UMBHealthComponent>(Component);
	if(!HealthComponent)
	{
		return 0.0f;
	}

	return HealthComponent->GetHealthPercent();
}
