#pragma once

#include "CoreMinimal.h"
#include "Engine/AssetManager.h"
#include "MBAssetManager.generated.h"

UCLASS()
class MODERNBLOW_API UMBAssetManager : public UAssetManager
{
	GENERATED_BODY()

public:
	virtual void StartInitialLoading() override;
};
