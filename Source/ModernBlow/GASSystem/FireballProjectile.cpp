#include "GASSystem/FireballProjectile.h"

#include "MBBaseCharacter.h"
#include "Components/SphereComponent.h"
#include "Engine/DamageEvents.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/KismetSystemLibrary.h"

AFireballProjectile::AFireballProjectile()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	AActor::SetLifeSpan(10.0f);
	
	Sphere = CreateDefaultSubobject<USphereComponent>("Sphere");
	SetRootComponent(Sphere);
	Sphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Sphere->SetCollisionResponseToAllChannels(ECR_Ignore);
	Sphere->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Overlap);
	Sphere->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Overlap);
	Sphere->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovement");
	ProjectileMovement->InitialSpeed = 1000.0f;
	ProjectileMovement->MaxSpeed = 1000.0f;
	ProjectileMovement->ProjectileGravityScale = 0.0f;
}

void AFireballProjectile::BeginPlay()
{
	Super::BeginPlay();
	Sphere->OnComponentBeginOverlap.AddDynamic(this, &AFireballProjectile::OnSphereOverlap);
}

void AFireballProjectile::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//UKismetSystemLibrary::PrintString(this, FString("Has been Overlapped"), true, true, FLinearColor::Yellow, 3.0f);
	FPointDamageEvent DamageEvent;
	DamageEvent.HitInfo = SweepResult;
	DamageEvent.DamageTypeClass = DamageTypeClass;

	/*AMBBaseCharacter* CharacterOwner = OtherActor;
	AController* Controller = CharacterOwner->GetController();*/
	if(IsValid(OtherActor) && Cast<AMBBaseCharacter>(OtherActor))
	{
		OtherActor->TakeDamage(50.0f, DamageEvent, OtherActor->GetOwner()->GetInstigatorController(), OtherActor->GetOwner());
		Destroy();
	}
}


