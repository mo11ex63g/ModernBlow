#include "GASSystem/MBProjectileSpell.h"

#include "FireballProjectile.h"
#include "Kismet/KismetSystemLibrary.h"

void UMBProjectileSpell::ActivateAbility(const FGameplayAbilitySpecHandle Handle,
                                         const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo,
                                         const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	//UKismetSystemLibrary::PrintString(this, FString("Has been activated"), true, true, FLinearColor::Red, 3.0f);

	bool bIsServer = HasAuthority(&ActivationInfo);
	
	if(!bIsServer)
	{
		UKismetSystemLibrary::PrintString(this, FString("!bIsServer"), true, true, FLinearColor::Red, 3.0f);
		return;
	}

	FVector ActorLocation = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
	
	FVector SpawnLocation = FVector(
		ActorLocation.X + 200.0f,
		ActorLocation.Y,
		ActorLocation.Z + 3.0f);
	
	FTransform SpawnTransform;
	SpawnTransform.SetLocation(SpawnLocation);
	
	AFireballProjectile* Projectile = GetWorld()->SpawnActorDeferred<AFireballProjectile>(
		ProjectileClass,
		SpawnTransform,
		GetOwningActorFromActorInfo(),
		Cast<APawn>(GetOwningActorFromActorInfo()),
		ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

	//Projectile->FinishSpawning(SpawnTransform);
	
}
