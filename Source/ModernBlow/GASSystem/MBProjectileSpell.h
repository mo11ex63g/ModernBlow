#pragma once

#include "CoreMinimal.h"
#include "GASSystem/Character/Abilities/CharacterGameplayAbility.h"
#include "MBProjectileSpell.generated.h"

class AFireballProjectile;

UCLASS()
class MODERNBLOW_API UMBProjectileSpell : public UCharacterGameplayAbility
{
	GENERATED_BODY()

protected:
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AFireballProjectile> ProjectileClass;
};
