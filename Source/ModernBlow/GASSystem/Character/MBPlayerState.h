#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemInterface.h"
#include "GameplayTagContainer.h"
#include "Abilities/AttributeSets/CharacterAttributeSetBase.h"
#include "GameFramework/PlayerState.h"
#include "MBPlayerState.generated.h"

class UMBHealthComponent;
class UCharacterAttributeSetBase;
class UCharacterAbilitySystemComponent;

UCLASS()
class MODERNBLOW_API AMBPlayerState : public APlayerState, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	AMBPlayerState();

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UCharacterAttributeSetBase* GetAttributeSetBase() const;

	UFUNCTION(BlueprintCallable, Category = "MBPlayerState")
	bool IsAlive() const;

	UFUNCTION(BlueprintCallable, Category = "MBPlayerState|UI")
	void ShowAbilityConfirmCancelText(bool ShowText);

	UFUNCTION(BlueprintCallable, Category = "MBPlayerState|Attributes")
	float GetHealth() const;
	
	UFUNCTION(BlueprintCallable, Category = "MBPlayerState|Attributes")
	float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, Category = "MBPlayerState|Attributes")
	float GetMana() const;
	
	UFUNCTION(BlueprintCallable, Category = "MBPlayerState|Attributes")
	float GetMaxMana() const;

	UFUNCTION(BlueprintCallable, Category = "MBPlayerState|Attributes")
	int32 GetCharacterLevel() const;

protected:
	UPROPERTY()
	UCharacterAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY()
	UCharacterAttributeSetBase* AttributeSetBase;

	FGameplayTag DeadTag;

	FDelegateHandle HealthChangedDelegateHandle;
	FDelegateHandle ManaChangedDelegateHandle;
	FDelegateHandle MaxHealthChangedDelegateHandle;
	FDelegateHandle MaxManaChangedDelegateHandle;
	FDelegateHandle CharacterLevelChangedDelegateHandle;

	virtual void BeginPlay() override;

	virtual void HealthChanged(const FOnAttributeChangeData& Data);
	virtual void MaxHealthChanged(const FOnAttributeChangeData& Data);
	virtual void ManaChanged(const FOnAttributeChangeData& Data);
	virtual void MaxManaChanged(const FOnAttributeChangeData& Data);
	virtual void CharacterLevelChanged(const FOnAttributeChangeData& Data);

	virtual void StunTagChanged(const FGameplayTag CallbackTag, int32 NewCount);
};
