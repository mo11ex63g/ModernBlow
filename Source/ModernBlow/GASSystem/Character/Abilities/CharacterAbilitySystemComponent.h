#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "CharacterAbilitySystemComponent.generated.h"

enum class EMBAbilityID : uint8;
class UInputAction;
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FReceivedDamageDelegate, UAbilitySystemComponent*, SourceASC, float,
                                               UnmitigatedDamage, float, MitigatedDamage);

UCLASS()
class MODERNBLOW_API UCharacterAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()

public:
	static UCharacterAbilitySystemComponent* GetAbilitySystemComponentFromActor(const AActor* Actor, bool LookForComponent = false);
	bool CharacterAbilitiesGiven = false;
	bool StartupsEffectsApplied = false;

	void AbilityInputTagHold(const FGameplayTag& InputTag);
	void AbilityInputTagReleased(const FGameplayTag& InputTag);

	void AddCharacterAbilities(const TArray<TSubclassOf<class UCharacterGameplayAbility>>& StartupAbilities);
	
	FReceivedDamageDelegate ReceivedDamage;
	
	virtual void ReceiveDamage(UAbilitySystemComponent* SourceASC, float UnmitigatedDamage, float MitigatedDamage);
};
