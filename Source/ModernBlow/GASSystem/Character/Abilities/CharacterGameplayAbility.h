#pragma once

#include "CoreMinimal.h"
#include "ModernBlowTypes.h"
#include "Abilities/GameplayAbility.h"
#include "CharacterGameplayAbility.generated.h"

enum class EMBAbilityID : uint8;

UCLASS()
class MODERNBLOW_API UCharacterGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:
	//UCharacterGameplayAbility();

	UPROPERTY(EditDefaultsOnly, Category = "Ability")
	FGameplayTag StartupinputTag;
	
	/*UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Ability")
	EMBAbilityID AbilityInputID = EMBAbilityID::None;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Ability")
	EMBAbilityID AbilityID = EMBAbilityID::None;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Ability")
	bool ActivateAbilityOnGranted = false;

	virtual void OnAvatarSet(const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilitySpec& Spec) override;*/
};
