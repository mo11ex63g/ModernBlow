#include "GASSystem/Character/Abilities/CharacterAbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"
#include "CharacterGameplayAbility.h"

UCharacterAbilitySystemComponent* UCharacterAbilitySystemComponent::GetAbilitySystemComponentFromActor(
	const AActor* Actor, bool LookForComponent)
{
	return Cast<UCharacterAbilitySystemComponent>(UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(Actor, LookForComponent));
}

void UCharacterAbilitySystemComponent::AbilityInputTagHold(const FGameplayTag& InputTag)
{
	if(!InputTag.IsValid()) return;

	TArray<FGameplayAbilitySpec>& ArrayOfActivatableAbilities = GetActivatableAbilities();
	
	for(FGameplayAbilitySpec& AbilitySpec : ArrayOfActivatableAbilities)
	{
		if(AbilitySpec.DynamicAbilityTags.HasTagExact(InputTag))
		{
			AbilitySpecInputPressed(AbilitySpec);
			if(!AbilitySpec.IsActive())
			{
				TryActivateAbility(AbilitySpec.Handle);
			}
		}
	}
}

void UCharacterAbilitySystemComponent::AbilityInputTagReleased(const FGameplayTag& InputTag)
{
	if(!InputTag.IsValid()) return;

	for(auto& AbilitySpec : GetActivatableAbilities())
	{
		if(AbilitySpec.DynamicAbilityTags.HasTagExact(InputTag))
		{
			AbilitySpecInputReleased(AbilitySpec);
		}
	}
}

void UCharacterAbilitySystemComponent::AddCharacterAbilities(
	const TArray<TSubclassOf<UCharacterGameplayAbility>>& StartupAbilities)
{
	for(const TSubclassOf<UCharacterGameplayAbility>& StartupAbility : StartupAbilities)
	{
		/*AbilitySystemComponent->GiveAbility(
			FGameplayAbilitySpec(StartupAbility, GetAbilityLevel(StartupAbility.GetDefaultObject()->AbilityID), static_cast<int32>(StartupAbility.GetDefaultObject()->AbilityInputID), this));
		UE_LOG(LogTemp, Warning, TEXT("Ability name: %s"), *StartupAbility.GetDefaultObject()->GetName());*/

		FGameplayAbilitySpec AbilitySpec = FGameplayAbilitySpec(StartupAbility, 1);
		if(const UCharacterGameplayAbility* MBAbility = Cast<UCharacterGameplayAbility>(AbilitySpec.Ability))
		{
			AbilitySpec.DynamicAbilityTags.AddTag(MBAbility->StartupinputTag);
			GiveAbility(AbilitySpec);
		}
	}
}

void UCharacterAbilitySystemComponent::ReceiveDamage(UAbilitySystemComponent* SourceASC, float UnmitigatedDamage,
                                                     float MitigatedDamage)
{
	ReceivedDamage.Broadcast(SourceASC, UnmitigatedDamage, MitigatedDamage);
}


