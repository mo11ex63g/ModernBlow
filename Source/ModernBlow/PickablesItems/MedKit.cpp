#include "PickablesItems/MedKit.h"

#include "MBHealthComponent.h"
#include "MBPLayerCharacter.h"

bool UMedKit::Consume(AMBPlayerCharacter* ConsumeTarget)
{
	UMBHealthComponent* CharacterHealthComponent = ConsumeTarget->GetHealthComponent();
	CharacterHealthComponent->AddHealth(Health);
	return true;
}
