#include "PickablesItems/PickablePowerups.h"
#include "InventoryItem.h"
#include "MBDataTableUtils.h"
#include "MBPLayerCharacter.h"

APickablePowerups::APickablePowerups()
{
	PowerupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PowerupMesh"));
	SetRootComponent(PowerupMesh);
}

void APickablePowerups::Interact(AMBPlayerCharacter* Character)
{
	FItemTableRow* ItemData = MBDataTableUtils::FindInventoryItemData(GetDataTableID());

	if(ItemData == nullptr)
	{
		UE_LOG(LogClass, Warning, TEXT("Not Found inventory item"));
		return;
	}

	TWeakObjectPtr<UInventoryItem> Item = TWeakObjectPtr<UInventoryItem>(NewObject<UInventoryItem>(Character, ItemData->InventoryItemClass));
	Item->Initialize(DataTableID, ItemData->InventoryItemDescription);

	const bool bPickedUp = Character->PickupItem(Item);

	if(bPickedUp)
	{
		Destroy();
	}
}

FName APickablePowerups::GetActionEventName() const
{
	return ActionInteract;
}
