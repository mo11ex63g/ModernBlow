#pragma once

#include "CoreMinimal.h"
#include "PickubleItem.h"
#include "PickablePowerups.generated.h"

UCLASS(Blueprintable)
class MODERNBLOW_API APickablePowerups : public APickubleItem
{
	GENERATED_BODY()

public:
	APickablePowerups();
	
	virtual void Interact(AMBPlayerCharacter* Character) override;
	virtual FName GetActionEventName() const override;

protected:
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* PowerupMesh;

private:
	FName ActionInteract = FName("Interact");
};
