#pragma once

#include "CoreMinimal.h"
#include "InventoryItem.h"
#include "WeaponInventoryItem.generated.h"

class AEquipableItem;

UCLASS()
class MODERNBLOW_API UWeaponInventoryItem : public UInventoryItem
{
	GENERATED_BODY()

public:
	UWeaponInventoryItem();
	
	void SetEquipWeaponClass(TSubclassOf<AEquipableItem>& WeaponClass);
	TSubclassOf<AEquipableItem> GetEquipWeaponClass() const;

	virtual bool Consume(AMBPlayerCharacter* ConsumeTarget) override;
	
protected:
	TSubclassOf<AEquipableItem> EquipWeaponClass;
};
