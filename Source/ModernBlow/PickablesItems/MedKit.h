#pragma once

#include "CoreMinimal.h"
#include "InventoryItem.h"
#include "MedKit.generated.h"

UCLASS()
class MODERNBLOW_API UMedKit : public UInventoryItem
{
	GENERATED_BODY()

public:
	virtual bool Consume(AMBPlayerCharacter* ConsumeTarget) override;
	
protected:
	UPROPERTY(EditDefaultsOnly, Category = "Medkit")
	float Health = 25.0f;
};
