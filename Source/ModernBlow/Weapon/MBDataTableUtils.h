#pragma once

#include "CoreMinimal.h"

struct FItemTableRow;
struct FWeaponTableRow;
struct FTableRowBase;

namespace MBDataTableUtils
{
	FWeaponTableRow* FindWeaponData(const FName WeaponID);
	FItemTableRow* FindInventoryItemData(const FName ItemID);
};
