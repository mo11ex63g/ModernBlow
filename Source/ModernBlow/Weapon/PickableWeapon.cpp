#include "PickableWeapon.h"
#include "MBDataTableUtils.h"
#include "MBPLayerCharacter.h"
#include "ModernBlow/InventoryItem.h"
#include "PickablesItems/WeaponInventoryItem.h"

APickableWeapon::APickableWeapon()
{
	WeaponMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponMesh"));
	SetRootComponent(WeaponMesh);
}

void APickableWeapon::Interact(AMBPlayerCharacter* Character)
{
	FWeaponTableRow* WeaponRow = MBDataTableUtils::FindWeaponData(DataTableID);
	if(WeaponRow)
	{
		TWeakObjectPtr<UWeaponInventoryItem> Weapon = NewObject<UWeaponInventoryItem>(Character);
		Weapon->Initialize(DataTableID, WeaponRow->WeaponItemDescription);
		Weapon->SetEquipWeaponClass(WeaponRow->EquipableItem);
		Character->PickupItem(Weapon);
		Destroy();
	}
}

FName APickableWeapon::GetActionEventName() const
{
	return ActionInteract;
}
