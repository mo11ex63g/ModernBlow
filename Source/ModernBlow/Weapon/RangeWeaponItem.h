#pragma once

#include "CoreMinimal.h"
#include "EquipableItem.h"
#include "RangeWeaponItem.generated.h"

//DECLARE_MULTICAST_DELEGATE_OneParam(FOnAmmoChanged, int32);

UENUM(BlueprintType)
enum EWeaponFireMode : uint8
{
	Single,
	FullAuto
};

UCLASS(Blueprintable)
class MODERNBLOW_API ARangeWeaponItem : public AEquipableItem
{
	GENERATED_BODY()

public:
	ARangeWeaponItem();
	void StartFire();
	void StopFire();
	
	FTransform GetForGripTransform() const;

	void StartAim();
	void StopAim();
	
	float GetAimFOV() const;
	float GetAimMovementMaxSpeed() const;

	/*int32 GetAmmo() const { return Ammo; }
	void SetAmmo(int32 NewAmmo);
	bool CanShoot() const { return Ammo > 0; }*/

	//FOnAmmoChanged OnAmmoChanged;
	
protected:
	virtual void BeginPlay() override;;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class UWeaponBarellComponent* WeaponBarell;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class UPickUpComponent* PickUpComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loadout")
	FName SocketWeaponBarell  = FName("Barrel");

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loadout")
	FName SocketWeaponGrip  = FName("Grip");

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loadout")
	FName SocketCharacterWeapon  = FName("CharacterWeapon");
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation | Weapon")
	UAnimMontage* WeaponFireMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation | Character")
	UAnimMontage* CharacterFireMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Parametrs", meta=(ClampMin=0.0f, UIMin=0.0f))
	float RateOfFire = 600.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Parametrs")
	TEnumAsByte<EWeaponFireMode> WeaponFireMode = EWeaponFireMode::Single;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Parametrs", meta=(ClampMin=0.0f, UIMin=0.0f, ClampMax = 2.0f, UIMax = 2.0f))
	float SpreadAngle = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Parametrs | Aiming", meta=(ClampMin=0.0f, UIMin=0.0f, ClampMax = 2.0f, UIMax = 2.0f))
	float AimSpreadAngle = 0.25f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Parametrs | Aiming", meta=(ClampMin=0.0f, UIMin=0.0f))
	float AimMovementMaxSpeed = 200.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Parametrs | Aiming", meta=(ClampMin=0.0f, UIMin=0.0f, ClampMax = 120.0f, UIMax = 120.0f))
	float AimFOV = 60.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Parametrs | Ammo", meta=(ClampMin=1, UIMin=1))
	int32 MaxAmmo = 30;
	
private:
	float GetShotTimerInterval();	
	float PlayAnimMontage(UAnimMontage* AnimMontage);
	void MakeShot();

	bool bIsAiming;

	float GetCurrentBulletSpread() const;

	//int32 Ammo = 0;
	
	FTimerHandle ShotTimer;
};
