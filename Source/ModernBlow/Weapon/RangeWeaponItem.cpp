#include "RangeWeaponItem.h"
#include "PickUpComponent.h"
#include "WeaponBarellComponent.h"

ARangeWeaponItem::ARangeWeaponItem()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("WeaponRoot"));

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->SetupAttachment(RootComponent);

	WeaponBarell = CreateDefaultSubobject<UWeaponBarellComponent>(TEXT("WeaponBarell"));
	WeaponBarell->SetupAttachment(WeaponMesh, SocketWeaponBarell);

	PickUpComponent = CreateDefaultSubobject<UPickUpComponent>(TEXT("PickUpComponent"));

	EquippedSocketName = SocketCharacterWeapon;
}

void ARangeWeaponItem::StartFire()
{
	if(bIsAiming)
	{
		MakeShot();
		if(WeaponFireMode == EWeaponFireMode::FullAuto)
		{
			GetWorld()->GetTimerManager().ClearTimer(ShotTimer);
			GetWorld()->GetTimerManager().SetTimer(ShotTimer, this, &ARangeWeaponItem::MakeShot, GetShotTimerInterval(), true);
		}
	}
}

void ARangeWeaponItem::StopFire()
{
	GetWorld()->GetTimerManager().ClearTimer(ShotTimer);
}

FTransform ARangeWeaponItem::GetForGripTransform() const
{
	return WeaponMesh->GetSocketTransform(SocketWeaponGrip);
}

void ARangeWeaponItem::StartAim()
{
	bIsAiming = true;
}

void ARangeWeaponItem::StopAim()
{
	bIsAiming = false;
}

float ARangeWeaponItem::GetAimFOV() const
{
	return AimFOV;
}

float ARangeWeaponItem::GetAimMovementMaxSpeed() const
{
	return AimMovementMaxSpeed;
}

/*void ARangeWeaponItem::SetAmmo(int32 NewAmmo)
{
	Ammo = NewAmmo;
	if(OnAmmoChanged.IsBound())
	{
		OnAmmoChanged.Broadcast(Ammo);
	}
}*/

void ARangeWeaponItem::BeginPlay()
{
	Super::BeginPlay();
	//SetAmmo(MaxAmmo);
}

float ARangeWeaponItem::GetShotTimerInterval()
{
	return 60.0f / RateOfFire;
}

float ARangeWeaponItem::PlayAnimMontage(UAnimMontage* AnimMontage)
{
	UAnimInstance* WeaponAnimInstance = WeaponMesh->GetAnimInstance();
	return WeaponAnimInstance->Montage_Play(AnimMontage);
}

void ARangeWeaponItem::MakeShot()
{
	checkf(GetOwner()->IsA<AMBBaseCharacter>(), TEXT("ARangeWeaponItem::Fire() only character can be an owner of range weapon"));
	AMBBaseCharacter* CharacterOwner;
	CharacterOwner = StaticCast<AMBBaseCharacter*>(GetOwner());
		
	if(IsValid(CharacterFireMontage))
	{
		CharacterOwner->PlayAnimMontage(CharacterFireMontage);
		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Black, TEXT("Anima"));
	}

	if(IsValid(WeaponFireMontage))
	{
		PlayAnimMontage(WeaponFireMontage);
	}
	
	AController* Controller = CharacterOwner->GetController();
	if(!IsValid(Controller))
	{
		return;
	}

	FVector PlayerViewPoint;
	FRotator PlayerViewRotation;
	
	Controller->GetPlayerViewPoint(PlayerViewPoint, PlayerViewRotation);

	FVector ViewDirection = PlayerViewRotation.RotateVector(FVector::ForwardVector);

	//SetAmmo(Ammo - 1);
	WeaponBarell->Shot(PlayerViewPoint, ViewDirection, Controller, GetCurrentBulletSpread());
}

float ARangeWeaponItem::GetCurrentBulletSpread() const
{
	float AngleInDegrees = bIsAiming ? AimSpreadAngle : SpreadAngle;
	return FMath::DegreesToRadians(AngleInDegrees);
}
