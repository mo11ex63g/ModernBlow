#include "WeaponBarellComponent.h"
#include "../Characters/Components/MBHealthComponent.h"
#include "Engine/DamageEvents.h"


void UWeaponBarellComponent::Shot(FVector ShotStart, FVector ShotDirection, AController* Controller, float SpredAngle)
{
	for(int i = 0; i < BulletsPerShot; i++)
	{
		ShotDirection += GetBulletSpreadOffset(FMath::RandRange(0.0f, SpredAngle), ShotDirection.ToOrientationRotator());
	
		FVector MuzzleLocation = GetComponentLocation(); 
		FVector ShotEnd = ShotStart + FiringRange * ShotDirection;

		FVector Line = ShotEnd - ShotDirection;
		
		FHitResult ShotResult;
		if(GetWorld()->LineTraceSingleByChannel(ShotResult, ShotStart, ShotEnd, ECC_GameTraceChannel11))
		{
			ShotEnd = ShotResult.ImpactPoint;
			DrawDebugSphere(GetWorld(), ShotEnd, 5.0f, 24, FColor::Red, false, 1.0f);

			AActor* HitActor = ShotResult.GetActor();
			if(IsValid(HitActor))
			{
				FPointDamageEvent DamageEvent;
				DamageEvent.HitInfo = ShotResult;
				DamageEvent.ShotDirection = ShotDirection;
				DamageEvent.DamageTypeClass = DamageTypeClass;
				
				HitActor->TakeDamage(DamageAmount, DamageEvent, Controller, GetOwner());
			}
		}
		DrawDebugLine(GetWorld(), MuzzleLocation, ShotEnd, FColor::Red, false, 1.0f, 0, 3.0f);
	}
}

FVector UWeaponBarellComponent::GetBulletSpreadOffset(float Angle, FRotator ShotRotation)
{
	float SpreadSize = FMath::Tan(Angle);
	float RotationAngle = FMath::RandRange(0.0f, 2 * PI);

	float SpreadY = FMath::Cos(RotationAngle);
	float SpreadZ = FMath::Sin(RotationAngle);

	FVector Result = (ShotRotation.RotateVector(FVector::UpVector) * SpreadZ + ShotRotation.RotateVector(FVector::RightVector) * SpreadY) * SpreadSize;

	return Result;
}
