#pragma once

#include "CoreMinimal.h"
#include "EquipableItem.h"
#include "ModernBlowTypes.h"
#include "Components/ActorComponent.h"
#include "CharacterEquipComponent.generated.h"


//DECLARE_MULTICAST_DELEGATE_OneParam(FOnCurrentWeaponAmmoChanged, int32)


class UEquipmentViewWidget;

class ARangeWeaponItem;

typedef TArray<class AEquipableItem*, TInlineAllocator<(uint32) EEquipmentSlots::MAX>> TItemsArray;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MODERNBLOW_API UCharacterEquipComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	EEquipableItemType GetCurrentEquippedItemType() const;

	ARangeWeaponItem* GetCurrentRangeWeapon() const;

	//FOnCurrentWeaponAmmoChanged OnCurrentWeaponAmmoChangedEvent;

	bool AddEquipmentItemToSlot(const TSubclassOf<AEquipableItem> EquipableItemClass, int32 SlotIndex);
	void RemoveItemFromSlot(int32 SlotIndex);

	void OpenViewEquipment(APlayerController* PlayerController);
	void CloseViewEquipment();
	bool IsViewVisible() const;

	const TArray<AEquipableItem*>& GetItems() const;

	void EquipItemInSlot(EEquipmentSlots Slot);

	void UnEquipCurrentItem();
	
	void EquipNextItem();
	void EquipPreviousItem();
	
protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loadout")
	TMap<EEquipmentSlots, TSubclassOf<class AEquipableItem>> ItemsLoadout;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Loadout")
	FName SocketCharacterWeapon  = FName("CharacterWeaponSocket");

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "View")
	TSubclassOf<UEquipmentViewWidget> ViewWidgetClass;

	void CreateViewWidget(APlayerController* PlayerController);
	
private:
	TArray<AEquipableItem*> ItemsArray;
	
	void CreateLoadout();

	uint32 NextItemsArraySlotIndex(uint32 CurrentSlotIndex);
	uint32 PreviousItemsArraySlotIndex(uint32 CurrentSlotIndex);

	EEquipmentSlots PreviousEquippedSlot;
	
	EEquipmentSlots CurrentEquippedSlot;
	AEquipableItem* CurrentEquippedItem;
	
	/*UFUNCTION()
	void OnCurrentWeaponAmmoChanged(int32 Ammo);*/
	
	ARangeWeaponItem* CurrentEquippedWeapon;

	TWeakObjectPtr<class AMBBaseCharacter> CachedBaseCharacter;

	UEquipmentViewWidget* ViewWidget;
};
