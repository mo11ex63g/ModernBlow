#pragma once

#include "CoreMinimal.h"
#include "../PickubleItem.h"
#include "PickableWeapon.generated.h"

UCLASS(Blueprintable)
class MODERNBLOW_API APickableWeapon : public APickubleItem
{
	GENERATED_BODY()

public:
	APickableWeapon();

	virtual void Interact(AMBPlayerCharacter* Character) override;
	virtual FName GetActionEventName() const override;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* WeaponMesh;

private:
	FName ActionInteract = FName("Interact");
};
