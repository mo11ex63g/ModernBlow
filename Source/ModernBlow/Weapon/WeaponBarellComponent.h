#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "WeaponBarellComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MODERNBLOW_API UWeaponBarellComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	void Shot(FVector ShotStart, FVector ShotDirection, AController* Controller, float SpredAngle);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Barell attributes")
	float FiringRange = 5000.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Barell attributes | Damage")
	float DamageAmount = 20.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Barell attributes | Damage")
	TSubclassOf<class UDamageType> DamageTypeClass;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Barell attributes", meta = (ClampMin = 1, UIMin = 1))
	int32 BulletsPerShot = 1;

private:
	FVector GetBulletSpreadOffset(float Angle, FRotator ShotRotation);

	TWeakObjectPtr<class AMBBaseCharacter> CachedBaseCharacter;
};
