// Copyright Epic Games, Inc. All Rights Reserved.

#include "ModernBlow.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ModernBlow, "ModernBlow" );
